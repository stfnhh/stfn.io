class CreateWorkTechnologiesJoinTable < ActiveRecord::Migration
  def change
    create_join_table :works, :technologies do |t|
      t.index [:work_id, :technology_id]
      t.index [:technology_id, :work_id]
    end
  end
end
