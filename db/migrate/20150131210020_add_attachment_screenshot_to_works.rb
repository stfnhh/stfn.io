class AddAttachmentScreenshotToWorks < ActiveRecord::Migration
  def self.up
    change_table :works do |t|
      t.attachment :screenshot
    end
  end

  def self.down
    remove_attachment :works, :screenshot
  end
end
