class HomeController < ApplicationController
  def index
    @works = Work.includes(:technologies).order(position: :asc).all
  end
end
