//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require jquery.waitforimages
//= require masonry
//= require_self


$(document).on('ready page:load', function(){
  $('#work').waitForImages(function(){
    $('#work').masonry({
      itemSelector: '.brick',
      columnWidth: '.brick'
    });
  });

  $('[data-toggle="tooltip"]').tooltip()
});
