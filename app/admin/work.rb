ActiveAdmin.register Work do
  config.filters = false
  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported

  sortable # creates the controller action which handles the sorting

  index do
    sortable_handle_column
    column :screenshot do |work|
      image_tag(work.screenshot.url(:thumbnail), width: 50, height: 50)
    end
    column :title
    actions defaults: true
  end


  show do |work|
    attributes_table do
      row :screenshot do
        image_tag(work.screenshot.url(:default), width: 300)
      end
      row :title
      row :url
      row :technologies do |n|
        n.technologies.map { |d| d.title }.join(", ").html_safe
      end
    end
  end




  form do |f|
    f.inputs "Post Details" do
    f.input :title
    f.input :url
    f.input :screenshot, as: :file
    f.inputs do
      f.input :technologies, as: :check_boxes
    end
  end
    f.actions
  end


  permit_params :title, :url, :color, :screenshot, technology_ids: []
end
