ActiveAdmin.register Technology do
  config.filters = false
  index do
    column :slug
    column :title
    actions defaults: true
  end


  show do |work|
    attributes_table do
      row :slug
      row :title
    end
  end

  form do |f|
    f.input :slug
    f.input :title
    f.actions
  end


  permit_params :slug, :title
end
