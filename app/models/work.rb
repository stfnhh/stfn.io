class Work < ActiveRecord::Base
  acts_as_list
  has_attached_file :screenshot, styles: { default: "600x", thumbnail: "100x100>" }, default_url: "/images/:style/missing.png", url: ":s3_alias_url", s3_host_alias: Rails.application.secrets.aws["bucket"], path: "work/:id/:style.:extension", s3_headers: { "Cache-Control" => "max-age=315576000", "Expires" => 10.years.from_now.httpdate }
  validates_attachment_content_type :screenshot, content_type: /\Aimage\/.*\Z/
  after_create :set_position

  has_and_belongs_to_many :technologies, order: 'slug DESC'
  accepts_nested_attributes_for :technologies, allow_destroy: true


  private
  def set_position
    self.position = Work.maximum(:position) || 1
  end

end
