class Technology < ActiveRecord::Base
  has_and_belongs_to_many :works
  default_scope { order(slug: 'asc') }
end
